/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.File;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author fodon
 */
public class Global {

    public static final String USER_AGENT = "Mozilla/5.0";
    public static final long SECOND = 1000L;
    public static final long MINUTE = 60 * SECOND;
    public static final long HOUR = 60 * MINUTE;
    public static final long DAY = 24 * HOUR;
    public static final long WEEK = 7 * DAY;
    public static final long MONTH = 30 * DAY;
    public static ObjectMapper mapper = getMapper();
    public static final DateTimeFormatter dayTimeFmtT = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");

    public static File getUsrHomeDir() {
        return new File(System.getProperty("user.home"));
    }

    /**
     * print nodes.
     *
     * @param name
     * @param root
     */
    public static void printNode(final String name, final JsonNode root) {
        try {
            System.out.println(name + ":Start ______________________");
            System.out.println(Global.mapper.writeValueAsString(root));
            System.out.println(name + ":Stop  ______________________");
        } catch (JsonProcessingException ex) {
            throw new Bug(ex);
        }
    }

    /**
     * Get the mapper.
     *
     * @return
     */
    private static ObjectMapper getMapper() {
        final ObjectMapper pmapper = new ObjectMapper();
        pmapper.enable(SerializationFeature.INDENT_OUTPUT);
        return pmapper;
    }

}
