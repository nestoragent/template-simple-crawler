/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.utils;

/**
 *
 * @author fodon
 */
public class Bug extends RuntimeException {

    public Bug() {
        this("Bug");
    }

    public Bug(String mesg, Exception ex) {
        super(mesg, ex);
    }

    public Bug(Exception ex) {
        super(ex);
    }

    public Bug(String mesg) {
        super(mesg);
    }
}
