package io.template.stepDefinitions;


import cucumber.api.java.After;
import io.template.lib.Init;

/**
 * Created by VelichkoAA on 14.01.2016.
 */
public class AfterTest {

    @After
    public void dispose() {
        Init.dispose();
    }
}
