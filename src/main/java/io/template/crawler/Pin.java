/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.template.crawler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.utils.Bug;
import io.utils.Global;
import java.util.HashSet;
import org.apache.logging.log4j.LogManager;

/**
 * This represents information about a pin. Sample link:
 * https://www.pinterest.com/pin/776871004441105142/
 *
 * @author fodon
 */
public class Pin extends PinMin {

    private static final long serialVersionUID = 1L;
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger();
    private int numRepins; // number of times this pin was repinned: (2)
    private String createAt;
    private String linkPinterest;
    private long id;
    private static final String PROD_PRE = "https://clothfix.com/products/";

    public Pin(long id, int numRepins, String title, String link, String text,
            String linkPinterest, String createdAt, String board, String img_url) {
        super(title, link, text, board, img_url);
        this.id = id;
        this.numRepins = numRepins;
        this.createAt = createdAt;
        this.linkPinterest = linkPinterest;
    }

    /**
     * Process JsonNode.
     *
     * @param item
     */
    public static Pin getPin(final JsonNode item) {
        try {
            final long id = item.get("id").asLong();
            final int repins = item.get("counts").get("repins").asInt();
            final String title = item.get("note").asText();
            final String link = item.get("original_link").asText();
            final String urlPinterest = item.get("url").asText();
            final String createdAt = item.get("created_at").asText();
            final String board_url = item.get("board").get("url").asText();
            final String[] units = board_url.split("/");
            final String board = units[3] + "/" + units[4];
            final String img_url = item.get("image").get("original").get("url").asText();
            String text;
            try {
                text = item.get("metadata").get("link").get("description").asText();
            } catch (Exception ex) {
                text = "blank";
            }
            return new Pin(id, repins, title, link, text, urlPinterest, createdAt, board, img_url);
        } catch (Exception ex) {
            try {
                String str = Global.mapper.writeValueAsString(item);
                System.out.println(str);
            } catch (JsonProcessingException pex) {
                throw new Bug(pex);
            }
            throw new Bug(ex);
        }
    }

    public JsonNode getJson() {
        ObjectNode out = Global.mapper.createObjectNode();
        out.put("id", getId());
        return out;
    }

    /**
     * Is Old.
     *
     * @param handles
     * @return
     */
    public boolean isOld(final HashSet<String> handles) {
        // Conditions to remove a pin.
        if (numRepins < 2) { // It has to have few repins.
            String link = getLink();
            // is a generic link.
            if (link.equals("http://clothfix.com/")) {
                return true;
            }

            // or if they are pointing to a collection.
            if (link.startsWith("https://clothfix.com/collections")) {
                return true;
            }

            // If the product is no longer at clothfix.
            if (link.startsWith(PROD_PRE)) { // if this is a product.
                final String handle = link.substring(PROD_PRE.length());
                if (!handles.contains(handle)) {
                    logger.info(handle);
                    return true;
                }
            }
        }
        return false;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public int getNumRepins() {
        return numRepins;
    }

    public void setNumRepins(int numRepins) {
        this.numRepins = numRepins;
    }

    public String getLinkPinterest() {
        return linkPinterest;
    }

    public void setLinkPinterest(String linkPinterest) {
        this.linkPinterest = linkPinterest;
    }

    @Override
    public String toString() {
        return super.toString()
                + ", id=" + id
                + ", numRepins=" + numRepins;
    }

    public long getId() {
        return id;
    }

    public static String[] getHeader() {
        return new String[]{
            "id",
            "title",
            "WebLink",
            "PinterestLink",
            "repins",
            "datetime"
        };
    }

    public String[] toCSV() {
        return new String[]{Long.toString(getId()),
            getTitle(),
            getLink(),
            getLinkPinterest(),
            Integer.toString(getNumRepins()),
            createAt
        };
    }

    /**
     * Load pins from CSV
     *
     * @param list
     * @return
     */
    public static Pin fromCSV(String[] list) {
        final long id = Long.parseLong(list[0]);
        final String title = list[1];
        final String link = list[2];
        final String pinterestLink = list[3];
        final int repins = Integer.parseInt(list[4]);
        final String createdAt = list[5];
        final String id_board = "";
        final String img_url = "";
        return new Pin(id, repins, title, link, pinterestLink, "", createdAt, id_board, img_url);
    }

}
