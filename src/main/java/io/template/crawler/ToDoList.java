/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.template.crawler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.template.lib.Init;
import io.template.lib.Props;
import io.utils.Bug;
import io.utils.Global;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

/**
 * @author fodon
 */
public class ToDoList {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger();
    public static final String CLOTH_FIX = "clothfix";

    public static void main(String[] args) throws Exception {
        final ToDoList todo = new ToDoList();

//        // DONE : print out profile details.
//        Profile prof = todo.getProfile("kaitlynmarry");
//        System.out.println(prof);
//        // TODO : Test of boards
//        final ArrayList<Board> boards = todo.getBoards("kaitlynmarry");
//        for (Board board : boards) {
//            System.out.println("url:" + board.getUrl());
//        }
//
//        // TODO : Test listing pins.
//        // NOTE: need only one page of pins.
//        final ArrayList<Pin> pins = todo.getPins(boards.get(0));
//        for (Pin pin : pins) {
//            System.out.println(pin.getLinkPinterest()); // should print out a few pins.
//        }
//
//         TODO : Add a comment to another user's pin.
//        todo.comment(pins.get(1), "Awesome !");
//         test that comment is listed on the pin.
//         TODO : Ensure that the number of likes on the profile goes up.
//        todo.like(pins.get(1));
//         Ensure that the target pin is liked
//
//
//        // DONE: Get a list of boards in my profile
        ArrayList<Board> myBoards = todo.getMyBoards();
        for (Board board : myBoards) {
            System.out.println(board);
        }
//
//        // DONE : pins a pin onto one of my boards.
//        final Pin chosenP = pins.get(0);
//        todo.repin(chosenP, myBoards.get(0));
//
//        // DONE: Get all my Pins
//        System.out.println("Locate added pin");
//        ArrayList<Pin> myPins = todo.getMyPins();
//        Pin myNewPin = myPins.get(0);
//        for (Pin pin : myPins) {
//            System.out.println(pin);
//            if (pin.getLink().equals(chosenP.getLink())) {
//                myNewPin = pin; break;
//            }
//        }
//        
//        // DONE: Delete recently added pin.
//        System.out.println("Delete added pin");
//        if (myNewPin != null) {
//            todo.unpin(myNewPin);
//        }
//        // DONE : Test follow/unfollow
//        todo.follow("clothfix");
//        todo.unfollow("clothfix");
//
//        // DONE : get a list of followers
//        ArrayList<Profile> list = todo.getMyFollow(Follow.followers);
//        for (Profile prof : list) {
//            System.out.println(prof);
//        }
//
        // DONE : get a list of following
        Set<Profile> list = todo.getMyFollow(Follow.following);
        for (Profile prof : list) {
            System.out.println(prof);
        }
//
    }

    /**
     * Given a profile name, return information about the profile.
     *
     * @param profName : profile name
     * @return : profile information.
     * @throws java.io.IOException
     */
    public Profile getProfile(final String profName) throws IOException {
        final String url = String.format("https://api.pinterest.com/v1/users/%s/?access_token=%s&fields=%s", profName, Props.get("pinterest.api.key"), "first_name,id,last_name,url,counts,created_at,bio,account_type");
        while (true) {
            final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setConnectTimeout((int) Global.MINUTE);
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", Global.USER_AGENT);
            final int code = con.getResponseCode();

            if (code == 429) { // IF request is rate limited.
                // BREAK and redo the request.
                try {
                    logger.info("Waiting 10 minutes");
                    Thread.sleep(Global.MINUTE * 10);
                } catch (InterruptedException ex) {
                    throw new Bug(ex);
                }
                continue; // Redo the fetch request.
            }

            final BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream())
            );

            final JsonNode node = Global.mapper.readValue(in, JsonNode.class);
            return Profile.create(profName, node.get("data"));
        }
    }

    /**
     * Delete pin.
     *
     * @param pin
     */
    public int deletePin(final Pin pin) {
        try {
            final String url = String.format("https://api.pinterest.com/v1/pins/%d/?access_token=%s",
                    pin.getId(), Props.get("pinterest.api.key"));
            final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setConnectTimeout((int) Global.MINUTE);
            con.setRequestMethod("DELETE");
            con.setRequestProperty("User-Agent", Global.USER_AGENT);

            return con.getResponseCode();
        } catch (IOException ex) {
            throw new Bug(ex);
        }
    }

    /**
     * Get a list of pins I have.
     *
     * @return
     */
    public ArrayList<Pin> getMyPins() {
        try {
            final ArrayList<Pin> pins = new ArrayList<>();
            String url = String.format("https://api.pinterest.com/v1/me/pins/?access_token=%s&fields=%s",
                    Props.get("pinterest.api.key"), "id,created_at,board,image,original_link,note,counts,metadata,url");
            while (true) {
                final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
                con.setConnectTimeout((int) Global.MINUTE);
                con.setRequestMethod("GET");
                con.setRequestProperty("User-Agent", Global.USER_AGENT);
                final BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream())
                );

                final JsonNode node = Global.mapper.readValue(in, JsonNode.class);
                for (JsonNode item : (ArrayNode) node.get("data")) {
                    pins.add(Pin.getPin(item));
                }

                JsonNode next = node.get("page").get("next");
                if (!next.isNull()) {
                    logger.info("Next:" + next.asText());
                    url = next.asText();
                } else {
                    logger.info("Done downloading pages");
                    break;
                }
            }
            return pins;

        } catch (IOException ex) {
            throw new Bug(ex);
        }
    }

    /**
     * Get a list of boards of the logged in user.
     *
     * @return
     */
    public ArrayList<Board> getMyBoards() {
        try {
            final String url = String.format("https://api.pinterest.com/v1/me/boards/?access_token=%s&fields=%s", Props.get("pinterest.api.key"), "id,name,counts");
            final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setConnectTimeout((int) Global.MINUTE);
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", Global.USER_AGENT);
            final BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream())
            );

            final JsonNode node = Global.mapper.readValue(in, JsonNode.class);
            final ArrayNode list = (ArrayNode) node.get("data");
            final ArrayList<Board> boards = new ArrayList<>();
            for (JsonNode but : list) {
                String name = but.get("name").asText();
                long id = but.get("id").asLong();
                int pins = but.get("counts").get("pins").asInt();
                Board board = new Board(name, id, pins);
                boards.add(board);
            }
            return boards;
        } catch (IOException ex) {
            throw new Bug(ex);
        }
    }

    /**
     * Get the list of boards in a profile.
     *
     * @param profName : profile name
     * @return : list of board identifiers.
     */
    public ArrayList<Board> getBoards(final String profName) {
        ArrayList<Board> result = new ArrayList<>();
        try {
            Init.getDriver().get("https://www.pinterest.com/" + profName + "/boards/");
            Init.getDriverExtensions().waitForPageToLoad();
            List<WebElement> boards = Init.getDriver().findElements(By.cssSelector("a.boardLinkWrapper"));
            if (boards.isEmpty()) {
                boards = Init.getDriver().findElements(By.cssSelector("div.ProfileBoardCard a"));
            }
            boards.forEach(board -> {
                String link = board.getAttribute("href");
                Connection.Response response = null;
                Connection con = null;
                String url = String.format("https://api.pinterest.com/v1/boards/%s?access_token=%s&fields=%s",
                        link.substring(link.indexOf(profName), link.length() - 1),
                        Props.get("pinterest.api.key"), "id,name,counts,url");
                try {
                    con = Jsoup.connect(url)
                            .timeout(120000)
                            .ignoreContentType(true);
                    response = con.execute();
                } catch (Exception e) {
                    System.err.println("On URL access: " + url);
                    System.err.println("Error Message: " + e.getMessage());
                }
                result.add(new Board(new JSONObject(response.body())));
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Create a pin.
     *
     * @param pin
     */
    public void createPin(PinMin pin) {
        // End point: https://api.pinterest.com/v1/pins/?access_token=Ac1q6fKdn9A5qzlXR2vnjF1fdvpHFI4gBevZhthDgdbtmEAojQAAAAA&fields=id%2Clink%2Cnote%2Curl
        // token: Ac1q6fKdn9A5qzlXR2vnjF1fdvpHFI4gBevZhthDgdbtmEAojQAAAAA
        // board: clothfix/top-10-designs
        // note: Number 1 design
        // link: https://clothfix.com/products/qpin-a-day-in-paradise-plaid-shirt
        // image_url: https://cdn.shopify.com/s/files/1/1451/4388/products/1_b3191c27-ffb5-4f2c-8805-c379cec5eb93.jpg?v=1476225981

        final String url = String.format("https://api.pinterest.com/v1/pins/?access_token=%s",
                Props.get("pinterest.api.key"));
        try {

            // act
            HttpPost post = new HttpPost(url);
            post.addHeader("User-Agent", Global.USER_AGENT);

            ArrayList<NameValuePair> postParameters = new ArrayList<>();
            postParameters.add(new BasicNameValuePair("board", pin.getBoard()));
            postParameters.add(new BasicNameValuePair("note", pin.getText()));
            postParameters.add(new BasicNameValuePair("link", pin.getLink()));
            postParameters.add(new BasicNameValuePair("image_url", pin.getImg_url()));

            post.setEntity(new UrlEncodedFormEntity(postParameters));

            HttpClient client = HttpClientBuilder.create().build();
            StatusLine status = client.execute(post).getStatusLine();

            // assert
            System.err.println("RESPONSE: " + status.getReasonPhrase());
            if (status.getStatusCode() != 200 // OK
                    && status.getStatusCode() != 201// Created
                    ) {
                throw new Bug("Error in POST");
            }
        } catch (IOException ex) {
            throw new Bug(ex);
        }
    }

    /**
     * Get Pins from boad.
     *
     * @param board
     * @return
     */
    public ArrayList<Pin> getPins2(Board board) {
        // https://api.pinterest.com/v1/boards/modcloth/married-in-modcloth/pins/?access_token=Ac1q6fKdn9A5qzlXR2vnjF1fdvpHFI4gBevZhthDgdbtmEAojQAAAAA
        try {
            final String url = String.format("https://api.pinterest.com/v1/boards/%s/%s/pins?access_token=%s&fields=%s",
                    "clothfix", board.getName(), Props.get("pinterest.api.key"), "id,created_at,board,image,original_link,note,counts,metadata,url");
            final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setConnectTimeout((int) Global.MINUTE);
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", Global.USER_AGENT);
            final BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream())
            );

            final JsonNode node = Global.mapper.readValue(in, JsonNode.class);
            final ArrayList<Pin> pins = new ArrayList<>();
            for (JsonNode pin : (ArrayNode) node.get("data")) {
                pins.add(Pin.getPin(pin));
            }
            return pins;
        } catch (IOException ex) {
            throw new Bug(ex);
        }
    }

    /**
     * Return ONE page of pins in a board.
     *
     * @param board : one of the boards returned from getBoards().
     * @return : list of board identifiers.
     */
    public ArrayList<Pin> getPins(Board board) {
        /**
         * https://api.pinterest.com/v1/boards/modcloth/married-in-modcloth/pins/?access_token=Ac1q6fKdn9A5qzlXR2vnjF1fdvpHFI4gBevZhthDgdbtmEAojQAAAAA
         */

        ArrayList<Pin> result = new ArrayList<>();
        loginIntoPinterest();
        try {
            Init.getDriver().get(board.getUrl());
            Init.getDriverExtensions().waitForPageToLoad();
            List<WebElement> pins = Init.getDriver().findElements(By.cssSelector("div.pinWrapper"));
            if (pins.isEmpty()) {
                pins = Init.getDriver().findElements(By.xpath("//a[contains(@href, '/pin/')]/../.."));
            }
            pins.forEach(pin -> {
                String link = pin.findElement(By.xpath(".//a[contains(@href, '/pin/')]")).getAttribute("href");
                link = link.split("pin")[2].replace("pin", "").replace("/", "").trim();
                Pin pinNew = null;
                Connection.Response response = null;
                Connection con = null;
                try {
                    con = Jsoup.connect(
                            String.format("https://api.pinterest.com/v1/pins/%s?access_token=%s&fields=%s",
                                    link, Props.get("pinterest.api.key"), "note,metadata,original_link,url"))
                            .timeout(120000)
                            .ignoreContentType(true);
                    response = con.execute();
                } catch (IOException e) {
                    System.err.println("Error when call a api. Message: " + e.getMessage());
                    System.err.println("try call again.");
                    try {
                        response = con.execute();
                    } catch (IOException e1) {
                        System.err.println("Error when call a api. Message: " + e1.getMessage());
                        con = null;
                    }
                }
                if (null == con) {
                    return;
                }
                JSONObject data = new JSONObject(response.body());
                String description;
                try {
                    description = data
                            .getJSONObject("data")
                            .getJSONObject("metadata")
                            .getJSONObject("article")
                            .get("description").toString();
                } catch (JSONException e) {
                    System.err.println("Api response didn't contains a description of a pin. Error: " + e.getMessage());
                    description = "empty";
                }
                JSONObject obj = data.getJSONObject("data");
                String createdAt = "";
                String img_url = "";
                String id_board = ""; // <id>/<board> ... combination used to define the board.
                pinNew = new Pin(Long.parseLong(obj.get("id").toString()),
                        convertToInt(pin.findElement(By.xpath(".//em[contains(@class, 'repinCountSmall')]")).getText()),
                        obj.get("note").toString(),
                        obj.get("original_link").toString(),
                        description,
                        obj.get("url").toString(),
                        createdAt,
                        id_board,
                        img_url);
                result.add(pinNew);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private int convertToInt(final String text) {
        String temp = text.replace("saves", "").replace(".", "").trim();
        try {
            if (text.contains("k") && text.contains(".")) {
                temp += "00";
            } else if (text.contains("k")) {
                temp += "0000";
            }
            temp = temp.replace("k", "").replace(".", "").trim();
            return Integer.parseInt(temp);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Login to pinterest.
     */
    public void loginIntoPinterest() {
        Init.getDriver().get("https://pinterest.com/login");
        try {
            Thread.sleep(5 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (Init.getDriver().findElements(By.cssSelector("button[type='submit']")).size() > 0) {
            WebElement email = Init.getDriver().findElement(
                    returnExist(new ArrayList<>(Arrays.asList(
                            By.cssSelector("input[name='username_or_email']"), By.cssSelector("input[name='id']")))));
            WebElement password = Init.getDriver().findElement(By.cssSelector("input[name='password']"));

            email.clear();
            email.sendKeys(Props.get("user.email"));
            password.clear();
            password.sendKeys(Props.get("user.password"));
            Init.getDriver().findElement(By.cssSelector("button[type='submit']")).click();
        }
        try {
            Thread.sleep(5 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Init.getDriverExtensions().waitForPageToLoad();
    }

    /**
     * Private return list.
     *
     * @param elements
     * @return
     */
    private By returnExist(ArrayList<By> elements) {
        Init.getDriverExtensions().waitForPageToLoad();
        for (By selector : elements) {
            if (Init.getDriver().findElements(selector).size() > 0
                    && Init.getDriver().findElements(selector).get(0).isDisplayed()) {
                return selector;
            }
        }
        return null;
    }

    /**
     * Get My followers.
     *
     * @param type
     * @return
     */
    public Set<Profile> getMyFollow(final Follow type) {
        final HashSet<Profile> profs = new HashSet<>();
        try {
            String url = String.format("https://api.pinterest.com/v1/me/%s/?access_token=%s&fields=%s",
                    type, Props.get("pinterest.api.key"),
                    "first_name,id,last_name,url,account_type,bio,counts,created_at,username");
            while (true) {
                final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
                con.setConnectTimeout((int) Global.MINUTE);
                con.setRequestMethod("GET");
                con.setRequestProperty("User-Agent", Global.USER_AGENT);
                final BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream())
                );

                final JsonNode node = Global.mapper.readValue(in, JsonNode.class);
                final ArrayNode list = (ArrayNode) node.get("data");

                for (JsonNode but : list) {
                    final Profile board = Profile.create(but.get("username").asText(), but);
                    profs.add(board);
                }
                JsonNode next = node.get("page").get("next");
                if (!next.isNull()) {
                    url = next.asText();
                } else {
                    break;
                }
            }
            return profs;
        } catch (IOException ex) {
            throw new Bug(ex);
        }
    }

    /**
     * Follow a profile.
     *
     * @param profName: name of profile to follow
     */
    public void follow(final String profName) {
        final String url = String.format("https://api.pinterest.com/v1/me/following/users/?access_token=%s", Props.get("pinterest.api.key"));
        try {
            final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setConnectTimeout((int) Global.MINUTE);
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", Global.USER_AGENT);

            con.setDoOutput(true);
            try (DataOutputStream oStream = new DataOutputStream(con.getOutputStream())) {
                oStream.writeBytes("user=" + profName);
                oStream.flush();
            }
            final int rcode = con.getResponseCode();
            System.out.println(rcode);
        } catch (IOException ex) {
            throw new Bug(ex);
        }
    }

    /**
     * Given a board, return one page of pins in it. ... NOT the complete list.
     *
     * @param profName: remove follower from profile.
     */
    public void unfollow(final String profName) {
        final String url = String.format("https://api.pinterest.com/v1/me/following/users/%s/?access_token=%s",
                profName, Props.get("pinterest.api.key"));
        try {
            final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setConnectTimeout((int) Global.MINUTE);
            con.setRequestMethod("DELETE");
            con.setRequestProperty("User-Agent", Global.USER_AGENT);

            final int rcode = con.getResponseCode();
            System.out.println(rcode);
        } catch (IOException ex) {
            throw new Bug(ex);
        }
    }

    /**
     * Comment on a pin by focusing on a pin and adding a comment to the
     * comments section.
     *
     * @param pin : pin to comment on.
     * @param comment : string of the comment.
     */
    public void comment(final Pin pin, String comment) {
        Init.getDriver().get(pin.getLinkPinterest());
        Init.getDriverExtensions().waitForPageToLoad();
        if (!Init.getDriver().findElement(By.cssSelector("form.addCommentForm textarea")).isDisplayed()) {
            click_by_js(Init.getDriver().findElement(By.cssSelector("span.commentDropdownIcon")));
        }

        Init.getDriver().findElement(By.cssSelector("form.addCommentForm textarea")).sendKeys(comment);
        click_by_js(Init.getDriver().findElement(By.cssSelector("button.addComment")));
        Init.getDriverExtensions().waitForPageToLoad();
    }

    /**
     * Like a pin by focusing on a pin and clicking the heart button on top of
     * the pin.
     *
     * @param pin : the pin to like.
     */
    public void like(final Pin pin) {
        loginIntoPinterest();
        Init.getDriver().get(pin.getLinkPinterest());
        Init.getDriverExtensions().waitForPageToLoad();
        if (!Init.getDriver().findElement(By.cssSelector("div.repinLike button.LikeButton"))
                .getAttribute("class").contains("unlike")) {
            Init.getDriver().findElement(By.cssSelector("div.repinLike button.LikeButton")).click();
        } else {
            System.out.println("Already liked");
        }
        Init.getDriverExtensions().waitForPageToLoad();
    }

    /**
     * Pin a pin on a board. This will put the pin on one of our boards. the
     * pin.
     *
     * @param pin : a pin object.
     * @param board
     */
    public void repin(final Pin pin, final Board board) {
        loginIntoPinterest();
        try {
            Init.getDriver().get(pin.getLinkPinterest());
            Init.getDriverExtensions().waitForPageToLoad();
            Init.getDriver().findElement(By.cssSelector("button.repin")).click();
            Init.getDriverExtensions().waitForPageToLoad();
            new WebDriverWait(Init.getDriver(), 10).until(
                    ExpectedConditions.presenceOfElementLocated(By.cssSelector("button.repinBtn")));
            List<WebElement> names = Init.getDriver().findElements(By.cssSelector("div.BoardLabel span.name"));
            WebElement saveParent = names.stream().filter(el -> el.getText().contains(board.getName())).findFirst().orElse(null);
            Assert.assertNotNull("Didn't found board with nams: " + board.getName(), saveParent);
            WebElement save = saveParent.findElement(By.xpath(".//../..//button[contains(@class, 'repinBtn')]"));
            new Actions(Init.getDriver()).moveToElement(save).click(save).build().perform();
            Init.getDriverExtensions().waitForPageToLoad();
        } catch (Exception e) {
            System.err.println("Error. message: " + e.getMessage());
        }
    }

    /**
     * UnPin a pin from a board. This will remove the pin from our board. pin.
     *
     * @param pin
     */
    public void unpin(final Pin pin) {
        final String url = String.format("https://api.pinterest.com/v1/pins/%d/?access_token=%s", pin.getId(), Props.get("pinterest.api.key"));
        try {
            final HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setConnectTimeout((int) Global.MINUTE);
            con.setRequestMethod("DELETE");
            con.setRequestProperty("User-Agent", Global.USER_AGENT);
            final int rcode = con.getResponseCode();
            if (rcode == 200) {
                System.out.println("Deleted pin");
            }
        } catch (IOException ex) {
            throw new Bug(ex);
        }
    }

    private void click_by_js(WebElement element) {
        ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].click();", element);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static enum Follow {
        following("/users"),
        followers("");

        String str;

        Follow(String str) {
            this.str = str;
        }

        @Override
        public String toString() {
            return this.name() + str;
        }
    }

}
