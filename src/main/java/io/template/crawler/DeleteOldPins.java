/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.template.crawler;

import com.fasterxml.jackson.databind.JsonNode;
import static io.template.crawler.ListMyPins.readPins;
import io.utils.Bug;
import io.utils.Global;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author fodon
 */
public class DeleteOldPins {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger();
    private static final File QPIN_HANDLES_FILE = new File(Global.getUsrHomeDir(), "git/data/qpin/handles.json");
    final HashSet<String> set = new HashSet<>();

    private DeleteOldPins() throws IOException {
        final JsonNode handles = Global.mapper.readTree(QPIN_HANDLES_FILE);

        for (JsonNode handle : handles) {
            set.add(handle.asText());
        }
        final ToDoList todo = new ToDoList();
        // Read pins from local disk.
        final ArrayList<Pin> list = readPins(ListProfiles.FileType.mypins);
        int incount = 0, outcount = 0;
        for (Pin pin : list) {
            if (pin.isOld(set)) {
                outcount++;
                int rcode = todo.deletePin(pin);
                if(rcode==404){
                    throw new Bug("Pin not found: Have latest pins been downloaded from pinterest?");
                }
            } else {
                incount++;
            }
        }
        logger.info(String.format("Incount:%d OutCount:%d", incount, outcount));

    }

    /**
     * Get the main string.
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        new DeleteOldPins();
    }
}
