package io.template.crawler;

import io.template.lib.Init;
import io.template.lib.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by nestor on 12.11.2016.
 */
public class Crawler extends Page {

    final String resultFilePath = "followers.txt";
    final ReentrantLock lock = new ReentrantLock();
    private final ExecutorService pool = Executors.newFixedThreadPool(2); // one for main thread, one for the pass by thread.
    List<String> followersResult = new ArrayList<>();

    public void crawlerFollowers(String name, int maxsteps) throws IOException, InterruptedException {
        Init.getDriver().get("https://www.pinterest.com/" + name + "/followers/");
        Init.getDriverExtensions().waitForPageToLoad();

//        final WebElement followers = Init.getDriver().findElement(By.cssSelector("a[href*='followers'] span.value"));
//        int followersCount = Integer.parseInt(followers.getText().replace(" ", "").replace(",", "").trim());
//        int count = 0;
//        int mCount = 0; // max count so far.
        System.out.println("time start scroll:" + LocalTime.now());

        try {
            for (int i = 0; i < maxsteps; i++) {
                scrollDown("10000");
//                System.out.print("Step:" + i + " Followers:" + count + "           \r");
                if (i % 100 == 0) {
                    final List<WebElement> list = Init.getDriver().findElements(By.cssSelector("a.userWrapper"));
                    System.out.println("list size: " + list.size());
                    System.out.println("list size: " + followersResult.size());
                    for (int j = followersResult.size(); j < list.size(); j++) {
//                    if (mCount < count) {
//                        pool.execute(new Writer(list, false));
                        followersResult.add(list.get(j).getAttribute("href").split("/")[3]);
                    }
                }

//                if (!lock.isLocked()) { // touching list is time consuming. Do it only when writer is available.
//                    final List<WebElement> list = Init.getDriver().findElements(By.cssSelector("a.userWrapper"));
//                    count = list.size();
//
//                    // Check if counts are same.
//                        mCount = count;
//                    } else if (mCount > count) { // something happened to the list we already fetched.
//                        throw new RuntimeException("Count reduced from " + mCount + " to " + count);
//                    }
//
//                    // Break if process is done.
//                    if ((count >= followersCount) || // going all teh way
//                            (maxsteps > 0 && i > maxsteps)) { // downloading a few steps.
//                        lock.lock();
//                        pool.execute(new Writer(list, true));
//                        break;
//                    }
//                }
//                Thread.sleep(500);
            }
        } catch (Exception e) {
            System.err.println("TimeoutException. Error:" + e.getMessage());
        }
        System.out.println("scroll end: " + LocalTime.now());

        System.out.println("write start: " + LocalTime.now());
        pool.execute(new Writer(followersResult, false));
        System.out.println("write end: " + LocalTime.now());
        System.out.println("end res: size: " + followersResult.size());
        System.out.println("Done downloading");
    }

    class Writer implements Runnable {

        final List<String> listx;
        boolean always; // run always

        Writer(final List<String> list, boolean guarantee) {
            this.listx = list;
            always = guarantee;
        }

        @Override
        public void run() {
            if (always) { // Write with certainity at end of process.
                lock.lock();
                write();
            } else if (lock.tryLock()) { // Update write: If another update is not writing.
                write();
            }
        }

        private void write() {
            try {
                final File logFile = new File(resultFilePath);
                StringBuffer sb = new StringBuffer();
                listx.parallelStream().forEach(name -> {
                    sb.append(name).append("\r\n");
                });

                try (final BufferedWriter writer = new BufferedWriter(new FileWriter(logFile))) {
                    writer.write(String.valueOf(sb));
                    writer.flush();
                } catch (Exception e) {
                    System.err.println("Something wrong with write to the file. Message:" + e.getMessage());
                }
            } finally {
                lock.unlock();
            }
        }
    }

}
