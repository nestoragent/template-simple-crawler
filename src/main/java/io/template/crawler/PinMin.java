/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.template.crawler;

import java.io.Serializable;

/**
 * Minimal pin definition to set it up.
 *
 * @author fodon
 */
public class PinMin implements Serializable {

    private static final long serialVersionUID = 1L;
    private String title; // title of the pin (Women Black handbag Casual tote)
    private String link; // destination link of the pin (https://clothfix.com/products/women-black-handbag-casual-tote)
    private String text; // text description of the pin (This genuine leather women's handbag is made of patchwork natural sheepskin. Keywords: shoulder bag, famous brand, women bag, casual tote sack. Genuine Leather: Sheepskin Lining Material: Polyester Measurements: Size: 14in wide x 6in thick x 16 tall; Shoulder Strap Length: About 116 cm)
    private String board; // link to board this pin is at.
    private String img_url;

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(final String img_url) {
        this.img_url = img_url;
    }

    public PinMin(final String title, String link, String text, String board, String img_url) {
        this.title = title;
        this.link = link;
        this.text = text;
        this.board = board;
        this.img_url = img_url;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return ", title='" + title
                + ", board='" + board
                + ", link='" + link
                + ", text='" + text;
    }

}
