/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.template.crawler;

import java.util.ArrayList;
import static io.template.crawler.ListProfiles.writeProfile;
import java.util.Set;

/**
 * List people I'm following.
 *
 * @author fodon
 */
public class ListMyFollowing {

    public static void main(String[] args) throws Exception {
        ToDoList todo = new ToDoList();
        final Set<Profile> list = todo.getMyFollow(ToDoList.Follow.following);
        writeProfile(false, ListProfiles.FileType.ifollowing, list);
    }
}
