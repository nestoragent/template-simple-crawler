/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.template.crawler;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import io.utils.Bug;
import io.utils.Global;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.apache.logging.log4j.LogManager;

/**
 * Get details of profiles.
 *
 * @author fodon
 */
public class ListProfiles {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger();
    private static File inFile = getFollowerFile("modcloth");

    /**
     * Get a list of followers.
     *
     * @param profile
     * @return
     */
    public static File getFollowerFile(final String profile) {
        return new File(Global.getUsrHomeDir(), String.format("/git/data/qpin/pinterest/brands/%s.txt", profile));
    }

    /**
     * Get the output file.
     *
     * @param out
     * @return
     */
    public static File getOutFile(final FileType out) {
        return new File(Global.getUsrHomeDir(), String.format("/git/data/qpin/pinterest/%s.txt", out.name()));
    }

    /**
     * Get a list of followers.
     *
     * @param after
     * @return
     */
    public static Set<Profile> getFollowerList(String after) {
        final ToDoList todo = new ToDoList();
        final Set<Profile> list = new HashSet<>();
        try (final FileReader fileReader = new FileReader(inFile);
                final BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            // FileReader reads text files in the default encoding.
            String line = null;
            boolean suppress = (after != null);
            while ((line = bufferedReader.readLine()) != null) {
                if (suppress) {
                    if (line.equals(after)) {
                        suppress = false;
                    }
                    continue;
                }
                final Profile prof = todo.getProfile(line);
                list.add(prof);
            }
            // Always close files.
        } catch (FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '"
                    + inFile + "'");
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file '"
                    + inFile + "'");
        }
        return list;
    }

    /**
     * Write a list of profiles and their properties.
     *
     * @param doAppend
     * @param out
     * @param list
     * @throws Exception
     */
    public static void writeProfile(final boolean doAppend, final FileType out,
            final Set<Profile> list) throws Exception {
        final File file = getOutFile(out);
        try (final CSVWriter writer = new CSVWriter(new FileWriter(file, doAppend), '\t')) {
            if (!doAppend) {
                writer.writeNext(new String[]{
                    "profName",
                    "likes",
                    "pins",
                    "following",
                    "followers",
                    "boards",
                    "pins",
                    "FirstName",
                    "LastName",
                    "AcctType",
                    "Bio"
                });
            }
            for (Profile prof : list) {
                // feed in your array (or convert your data to an array)
                final String[] entries = new String[]{prof.profName,
                    Integer.toString(prof.likes),
                    Integer.toString(prof.pins),
                    Integer.toString(prof.following),
                    Integer.toString(prof.followers),
                    Integer.toString(prof.boards),
                    Integer.toString(prof.pins),
                    prof.fName,
                    prof.lName,
                    prof.actType,
                    prof.bio};
                logger.info(prof);
                writer.writeNext(entries);
                writer.flush();
            }
        }
    }

    /**
     * Get a list of profiles.
     *
     * @param type
     * @return
     */
    public static Set<Profile> readProfiles(FileType type) {
        final File file = getOutFile(type);
        final Set<Profile> list = new HashSet<>();
        try (final CSVReader reader = new CSVReader(new FileReader(file), '\t')) {
            //String[] title = reader.readNext(); // hitargetrs doesn't have title line
            String[] line;
            while ((line = reader.readNext()) != null) {
                String profName = line[0];
                int likes = Integer.parseInt(line[1]);
                int repins = Integer.parseInt(line[2]);
                int following = Integer.parseInt(line[3]);
                int followers = Integer.parseInt(line[4]);
                int boards = Integer.parseInt(line[5]);
                //int repins = Integer.parseInt(line[6]);
                String fName = line[7];
                String lName = line[8];
                String actType = line[9];
                String bio = line[10];

                //                        String profName, String bio, String first_name,
                //            String last_name, String account_type, DateTime created_at,
                //            int pins, int following, int followers, int boards, int likes
                final Profile prof = new Profile(profName, bio, fName, lName, actType, null, repins, following, followers, boards, likes);
                list.add(prof);
            }
        } catch (Exception ex) {
            throw new Bug(ex);
        }
        return list;
    }

    public static enum FileType {
        hitargets, // target profiles.
        ifollowing, // who I'm following
        mypins, // pins in my profile
        acts; // list of actions by profile.
    }

    public static void main(final String[] args) throws Exception {
        String bookmark = "jakekasperski9";// When continuing after a break
        //String bookmark = null;// when starting from the start.
        final Set<Profile> list = getFollowerList(bookmark);
        writeProfile(bookmark != null, FileType.hitargets, list);
        logger.info("Testing logging");
    }

}
