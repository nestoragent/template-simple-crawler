/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.template.crawler;

import org.json.JSONObject;

/**
 * @author fodon
 */
public class Board {

    private String name;
    private String url;
    private long id;
    private int numPins;

    Board(final JSONObject data) {
        this.name = data.getJSONObject("data").get("name").toString();
        this.id = Long.parseLong(data.getJSONObject("data").get("id").toString());
        this.numPins = Integer.parseInt(data.getJSONObject("data").getJSONObject("counts").get("pins").toString());
        this.url = data.getJSONObject("data").get("url").toString();
    }

    public Board(final String name, final long id, final int pins) {
        this.numPins = pins;
        this.name = name;
        this.id = id;
    }

    public Board(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "name: " + name + ", id: " + id + " , numPins:" + numPins + ".";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumPins() {
        return numPins;
    }

    public void setNumPins(int numPins) {
        this.numPins = numPins;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
