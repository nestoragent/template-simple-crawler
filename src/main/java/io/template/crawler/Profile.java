/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.template.crawler;

import com.fasterxml.jackson.databind.JsonNode;
import io.utils.Global;
import java.util.Objects;
import org.joda.time.DateTime;

/**
 *
 * @author fodon
 */
public class Profile {

    final public int pins, followers, following, likes, boards;
    final public String profName, bio, fName, lName, actType;
    final public DateTime created_at;

    public Profile(String profName, String bio, String first_name,
            String last_name, String account_type, DateTime created_at,
            int pins, int following, int followers, int boards, int likes) {
        this.profName = profName;
        this.bio = bio;
        this.fName = first_name;
        this.lName = last_name;
        this.actType = account_type;
        this.created_at = created_at;
        this.pins = pins;
        this.following = following;
        this.followers = followers;
        this.boards = boards;
        this.likes = likes;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.profName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Profile other = (Profile) obj;
        if (!Objects.equals(this.profName, other.profName)) {
            return false;
        }
        return true;
    }

    public static Profile create(String profName, JsonNode data) {
        final JsonNode counts = data.get("counts");
        return new Profile(profName,
                data.get("bio").asText(),
                data.get("first_name").asText(),
                data.get("last_name").asText(),
                data.get("account_type").asText(),
                Global.dayTimeFmtT.parseDateTime(data.get("created_at").asText()),
                counts.get("pins").asInt(),
                counts.get("following").asInt(),
                counts.get("followers").asInt(),
                counts.get("boards").asInt(),
                counts.get("likes").asInt());
    }

    @Override
    public String toString() {
        return String.format("%s:%s : followers:%d following:%d pins:%d boards:%d likes:%d", actType, profName, followers, following, pins, boards, likes);
    }
}
