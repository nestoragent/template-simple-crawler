/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.template.crawler;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import io.template.crawler.ListProfiles.FileType;
import static io.template.crawler.ListProfiles.getOutFile;
import io.utils.Bug;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author fodon
 */
public class ListMyPins {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger();

    public static void writePins(final FileType out,
            final ArrayList<Pin> list) {
        final File file = getOutFile(out);
        try (final CSVWriter writer = new CSVWriter(new FileWriter(file), '\t')) {
            writer.writeNext(Pin.getHeader());
            for (Pin pin : list) {
                // feed in your array (or convert your data to an array)
                final String[] entries = pin.toCSV();
                writer.writeNext(entries);
                writer.flush();
            }

        } catch (Exception ex) {
            throw new Bug(ex);
        }
    }

    /**
     * Get a list of pins.
     *
     * @param out
     * @return
     * @throws java.io.IOException
     */
    public static ArrayList<Pin> readPins(final FileType out) {
        final ArrayList<Pin> list = new ArrayList<>();
        try {
            CSVReader reader = new CSVReader(new FileReader(getOutFile(out)), '\t');
            String[] line = reader.readNext();
            while ((line = reader.readNext()) != null) {
                // nextLine[] is an array of values from the line
                list.add(Pin.fromCSV(line));
            }
            return list;
        } catch (IOException ex) {
            throw new Bug(ex);
        }
    }

    public static void updatePins() {
        final ToDoList todo = new ToDoList();
        // Read pins from pinterest.
        final ArrayList<Pin> list = todo.getMyPins();
        writePins(FileType.mypins, list);
    }

    public static void main(String[] args) throws Exception {
        updatePins();
    }
}
